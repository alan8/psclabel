package psclabel

import "html/template"

// Output structure for PDF
type Output struct {
	Year     int
	Name     string
	SortName string
	Boat     string
	Berth    string
}

// UI global ui template
var UI *template.Template
