package pdf

import (
	"fmt"
	"log"
	"sort"
	"strings"
	"time"

	"github.com/jung-kurt/gofpdf"
	"gitlab.com/alan8/psclabel"
	"gitlab.com/alan8/psclabel/pkg/tempfile"
)

// PrintSingle build pdf into temp file
func PrintSingle(name, boat, berth string) (string, error) {
	// create file
	tmpfile, err := tempfile.New("", "psclabel_output_single_", ".pdf")
	if err != nil {
		return "", err
	}

	// build the pdf output - note done twice one for boat one for trailer
	t := time.Now()
	pdf := gofpdf.New("P", "mm", "A4", "")
	pdfContent(pdf, t.Year(), name, boat, berth)
	// two pages are required
	pdfContent(pdf, t.Year(), name, boat, berth)
	pdfOutput(pdf, tmpfile.Name())
	return tmpfile.Name(), nil
}

// PrintBulk process boat & berth data, combines them and loops through
// double lable production
func PrintBulk(berths, boats [][]string) (string, error) {
	var err error
	// create file
	tmpfile, err := tempfile.New("", "psclabel_output_single_", ".pdf")
	if err != nil {
		return "", err
	}

	t := time.Now()
	var boat, name string
	var nameparts []string
	var oneRecord psclabel.Output
	var allRecords []psclabel.Output
	var sd, ed time.Time
	for _, berth := range berths[1:] {
		sd, err = time.Parse("02/Jan/2006", berth[6])
		if err != nil {
			log.Fatal(err)
		}
		ed, err = time.Parse("02/Jan/2006", berth[7])
		if err != nil {
			log.Fatal(err)
		}
		if t.Before(ed) && t.After(sd) {
			boat, name = searchBoats(boats, berth[5])
			oneRecord.Year = t.Year()
			oneRecord.Name = name
			nameparts = strings.Split(name, " ")
			oneRecord.SortName = fmt.Sprintf("%s %s", nameparts[len(nameparts)-1], nameparts[0])
			oneRecord.Boat = boat
			oneRecord.Berth = fmt.Sprintf("%s%s", berth[3], berth[1])
			allRecords = append(allRecords, oneRecord)
		}
	}

	// sort comes here
	sort.Sort(bySurname(allRecords))

	// PDF output
	pdf := gofpdf.New("P", "mm", "A4", "")
	for _, label := range allRecords {
		pdfContent(pdf, label.Year, label.Name, label.Boat, label.Berth)
		pdfContent(pdf, label.Year, label.Name, label.Boat, label.Berth)
	}
	pdfOutput(pdf, tmpfile.Name())
	return tmpfile.Name(), err
}

// searchBoats searches the boats CSV matching on boat ID
// ignores first line
func searchBoats(records [][]string, boatid string) (string, string) {
	var boat string
	var name string
	for i, line := range records {
		if i > 0 {
			if line[0] == boatid {
				boat = line[1]
				if len(line[44]) > 0 {
					name = line[44]
				} else {
					name = line[33]
				}
				break
			}
		}
	}
	return boat, name
}

// pdfOutput writes the pdf file
func pdfOutput(pdf *gofpdf.Fpdf, o string) {
	err := pdf.OutputFileAndClose(o)
	if err != nil {
		log.Fatal(err)
	}
}

// pdfContent build the pdf content to specific layout and size
func pdfContent(pdf *gofpdf.Fpdf, year int, name string, boat string, berth string) {
	pdf.AddPageFormat("P", gofpdf.SizeType{Wd: 63, Ht: 43})
	pdf.SetMargins(5, 3, 3)
	pdf.SetAutoPageBreak(false, 1)
	pdf.SetFont("Arial", "B", 18)
	pdf.SetXY(5, 3)
	pdf.SetFillColor(0, 0, 0)
	pdf.SetTextColor(255, 255, 255)
	out := fmt.Sprintf("PSC Licence %4d", year)
	pdf.CellFormat(0, 12, out, "1", 0, "C", true, 0, "")
	pdf.Ln(12)
	pdf.SetTextColor(0, 0, 0)
	pdf.SetFont("Arial", "B", 15)
	out = fmt.Sprintf("%s\n%s\n%s", short(name, 16), boat, berth)
	pdf.MultiCell(0, 8, out, "1", "L", false)

}

// short truncates
func short(s string, i int) string {
	runes := []rune(s)
	if len(runes) > i {
		return string(runes[:i])
	}
	return s
}

// SurnameSorter for sort package
type bySurname []psclabel.Output

func (a bySurname) Len() int           { return len(a) }
func (a bySurname) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a bySurname) Less(i, j int) bool { return a[i].SortName < a[j].SortName }
