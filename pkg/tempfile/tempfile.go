// Package tempfile provides tools to create and delete temporary files
package tempfile

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

// New returns an unused filename for output files.
// from https://golang.org/src/cmd/pprof/internal/tempfile/tempfile.go
// Copyright 2014 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
func New(dir, prefix, suffix string) (*os.File, error) {
	for index := 1; index < 10000; index++ {
		path := filepath.Join(dir, fmt.Sprintf("%s%03d%s", prefix, index, suffix))
		if _, err := os.Stat(path); err != nil {
			return os.Create(path)
		}
	}
	// Give up
	return nil, fmt.Errorf("could not create file of the form %s%03d%s", prefix, 1, suffix)
}

var tempFilesMu = sync.Mutex{}

// Cleanup any PDFs in current directory  with refix and older than days
func Cleanup(dir, prefix string, days int) {
	t := time.Now()
	t = t.AddDate(0, 0, -1*days)
	tempFilesMu.Lock()
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		if strings.HasPrefix(file.Name(), prefix) {
			info, err := os.Stat(file.Name())
			if err != nil {
				log.Fatal(err)
			}
			if t.After(info.ModTime()) {
				os.Remove(file.Name())
			}
		}
	}

	tempFilesMu.Unlock()
}
