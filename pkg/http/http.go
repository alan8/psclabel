package http

import (
	"encoding/csv"
	"errors"
	"fmt"

	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.com/alan8/psclabel"
	"gitlab.com/alan8/psclabel/pkg/pdf"
	"gitlab.com/alan8/psclabel/pkg/tempfile"
)

// Page to be used in user interface web page
// output
type Page struct {
	Error     string
	Year      int
	Body      []byte
	Labelfile string
}

// chttp allow  serving of local static file
var chttp = http.NewServeMux()

// Run sets routes and starts server
func Run(port string) {

	chttp.Handle("/", http.FileServer(http.Dir("./")))

	http.HandleFunc("/", handler)

	log.Fatal(http.ListenAndServe(":"+port, nil))

}

// handler is the root page handler - this is the main user interface
func handler(w http.ResponseWriter, r *http.Request) {
	defer tempfile.Cleanup("./", "psclabel_output_", 1)
	if strings.Contains(r.URL.Path, ".") {
		// static type file with a dot
		chttp.ServeHTTP(w, r)
		return
	}
	// regular home page
	err := r.ParseMultipartForm(0)
	if err == http.ErrNotMultipart {
		err = nil
	}
	if err != nil {
		fmt.Fprint(w, err)
	}
	var labelfile string
	var errtext string
	if r.FormValue("form") == "single" {
		labelfile, err = validatesingle(w, r)
	}
	if r.FormValue("form") == "bulk" {
		labelfile, err = validatebulk(w, r)
	}
	if err != nil {
		errtext = err.Error()
	}
	t := time.Now()
	p := &Page{Error: errtext, Year: t.Year(), Labelfile: labelfile}
	err = psclabel.UI.Execute(w, p)
	if err != nil {
		log.Fatal(err)
	}
}

func validatesingle(w http.ResponseWriter, r *http.Request) (string, error) {
	var e string
	if len(r.Form["Name"][0]) < 3 {
		e = e + "Error Name too short: "
	}
	if len(r.Form["Boat"][0]) < 3 {
		e = e + "Error Boat too short: "
	}
	if len(r.Form["Berth"][0]) == 0 {
		e = e + "Error Berth can not be empty: "
	}
	if len(e) > 0 {
		return "", errors.New(e)
	}

	filename, err := pdf.PrintSingle(r.FormValue("Name"), r.FormValue("Boat"), r.FormValue("Berth"))

	return filename, err

}

func validatebulk(w http.ResponseWriter, r *http.Request) (string, error) {
	file1, _, err := r.FormFile("BerthFile")
	if err != nil {
		return "", errors.New("Mooring File: " + err.Error())
	}

	file2, _, err := r.FormFile("BoatFile")
	if err != nil {
		return "", errors.New("Boats File: " + err.Error())
	}

	berthfile := csv.NewReader(file1)
	berths, err := berthfile.ReadAll()
	if err != nil {
		return "", errors.New("Mooring File: Error reading all lines:: " + err.Error())
	}

	boatsfile := csv.NewReader(file2)
	boats, err := boatsfile.ReadAll()
	if err != nil {
		return "", errors.New("Boats File: Error reading all lines:: " + err.Error())
	}

	filename, err := pdf.PrintBulk(berths, boats)
	return filename, err
}
