# This is an excercse in reorgansing my Go Project

As far as I understand - it doesn't currently compile ... Nov 22

The original working project was a single file  see https://gitlab.com/alan8/psc-label/tree/master

There was no compelling need to re-organise as the code is fairly small and one file is OK, but it was an excercise in learning style & techniques

The code has been reorganised based on many of the recommendations in the following resources

* [Ben Johnson, Standard Package Layout](https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1#.edbt4x6e5)
* [Ben Johnson, Structuring Pakages in Go](https://medium.com/@benbjohnson/structuring-applications-in-go-3b04be4ff091#.ouawtmu5h)
* [Racyll, Style Guideline for Go Packages](https://rakyll.org/style-packages/)
* [Peter Bourgon, Go Best Practices 2016](https://peter.bourgon.org/go-best-practices-2016/#repository-structure)
* [William Kennedy, Going Go Programming](https://www.goinggo.net/)

Happy to take any comments and criticisms

Items I struggled with
* avoiding circular packages as I need the redirect to home page, in the end I left redirect out of my pkg/http
* not using a global variable for the Page structure, as workingout how to pass this through to handlers elluded me and solutions looked overly complex

## improvements from Gophers
* @acln  dont parse template in handler - parse in init https://play.golang.org/p/72Ylr-OzM5
* @acin  global Page variable gets shared between sessions, removed
* @acin  keep login of pdf production separate from form validation
* @acin parse template once only in init()
* @acin rename SurnameSorter to bySurname makes easier to read code
* @dlsniper @wizardofmaths removed redirect  ( this came naturally when functions structured better )

## My improvements
* created a tempfile package so uses dont step on each others files
* moved Error to single top of page message
* moved the template constant to main.go

## Other notes
To deploy to Heruko requires GoDeps, to run in this cmd/binary package structure  requires from project root `godep save ./cmd/...`
